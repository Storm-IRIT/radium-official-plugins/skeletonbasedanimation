
cmake_minimum_required(VERSION 3.6)
#------------------------------------------------------------------------------
# Project definition
# must set project name before doing anything else (in particular the install prefix test below)
project(SkeletonBasedAnimation)

#------------------------------------------------------------------------------
# Policies and global parameters for CMake
if (POLICY CMP0077)
	cmake_policy(SET CMP0077 NEW)
endif ()
set(CMAKE_DISABLE_SOURCE_CHANGES ON)
set(CMAKE_DISABLE_IN_SOURCE_BUILD ON)
# Be nice to visual studio
set_property(GLOBAL PROPERTY USE_FOLDERS ON)


# set the installation directory for all the apps at once. This will make the same definition ignore on all the
# included applications
if (CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT)
    set(CMAKE_INSTALL_PREFIX "${CMAKE_CURRENT_BINARY_DIR}/installed-${CMAKE_CXX_COMPILER_ID}" CACHE PATH
            "Install path prefix, prepended onto install directories." FORCE)
    message("Set install prefix to ${CMAKE_INSTALL_PREFIX}")
    set(CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT False)
endif ()

# Qt stuff
find_package(Radium REQUIRED Core Engine PluginBase GuiBase IO)
find_package(Qt5 COMPONENTS Core Widgets REQUIRED)
set(Qt5_LIBRARIES Qt5::Core Qt5::Widgets)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

set(sba_sources
	src/SkeletonComponent.cpp
	src/SkinningComponent.cpp
	src/SkeletonBasedAnimationSystem.cpp
	src/SkeletonBasedAnimationPlugin.cpp
	src/Drawing/SkeletonBoneDrawable.cpp
	src/UI/SkeletonBasedAnimationUI.cpp
        )

set(sba_headers
	src/SkeletonComponent.hpp
	src/SkinningComponent.hpp
	src/SkeletonBasedAnimationSystem.hpp
	src/SkeletonBasedAnimationPlugin.hpp
	src/SkeletonBasedAnimationPluginMacros.hpp
	src/Drawing/SkeletonBoneDrawable.hpp
	src/UI/SkeletonBasedAnimationUI.h
        )

set(sba_resources
	Resources/SkeletonBasedAnimation.qrc
	Resources/SkinningTextures.qrc
        )

set(sba_uis
	src/UI/SkeletonBasedAnimationUI.ui
        )
qt5_wrap_ui(anim_moc ${sba_uis})

set(CMAKE_INCLUDE_CURRENT_DIR ON)
include_directories(
        ${CMAKE_CURRENT_BINARY_DIR} # Moc
)

# Our library project uses these sources and headers.
add_library(
        ${PROJECT_NAME} SHARED
        ${sba_sources}
        ${sba_headers}
        ${sba_inlines}
        ${sba_markdowns}
        ${sba_uis}
        ${sba_resources}
    )

target_include_directories(${PROJECT_NAME} PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/src)
target_compile_definitions(${PROJECT_NAME} PRIVATE "-D${PROJECT_NAME}_EXPORTS")

target_link_libraries(${PROJECT_NAME} PUBLIC
        Radium::Core
        Radium::Engine
        Radium::PluginBase
        Radium::GuiBase
        ${Qt5_LIBRARIES}
        )

configure_radium_plugin(
        NAME ${PROJECT_NAME}
    INSTALL_IN_RADIUM_BUNDLE
)
