#include <SkeletonComponent.hpp>

#include <fstream>
#include <iostream>
#include <queue>

#include <Core/Animation/KeyFramedValueInterpolators.hpp>
#include <Core/Animation/Pose.hpp>
#include <Core/Asset/HandleToSkeleton.hpp>
#include <Core/Containers/AlignedStdVector.hpp>
#include <Core/Geometry/TriangleMesh.hpp>
#include <Core/Math/Math.hpp> // areApproxEqual

#include <Engine/Managers/ComponentMessenger/ComponentMessenger.hpp>
#include <Engine/Renderer/RenderObject/RenderObjectManager.hpp>

#include <Drawing/SkeletonBoneDrawable.hpp>

using KeyFramedValue = Ra::Core::Animation::KeyFramedValue<Ra::Core::Transform>;

using Ra::Core::Animation::HandleArray;
using Ra::Core::Animation::Skeleton;
using Ra::Core::Animation::WeightMatrix;
using Ra::Engine::ComponentMessenger;

using namespace Ra::Core::Utils; // log

namespace SkeletonBasedAnimationPlugin {

SkeletonComponent::SkeletonComponent( const std::string& name, Ra::Engine::Entity* entity ) :
    Component( name, entity ) {}

SkeletonComponent::~SkeletonComponent() {}

// Component interface

bool SkeletonComponent::canEdit( const Ra::Core::Utils::Index& roIdx ) const {
    // returns true if the roIdx is one of our bones.
    return (
        std::find_if( m_boneDrawables.begin(), m_boneDrawables.end(), [roIdx]( const auto& bone ) {
            return bone->getRenderObjectIndex() == roIdx;
        } ) != m_boneDrawables.end() );
}

Ra::Core::Transform SkeletonComponent::getTransform( const Ra::Core::Utils::Index& roIdx ) const {
    CORE_ASSERT( canEdit( roIdx ), "Transform is not editable" );
    const auto& bonePos =
        std::find_if( m_boneDrawables.begin(), m_boneDrawables.end(), [roIdx]( const auto& bone ) {
            return bone->getRenderObjectIndex() == roIdx;
        } );

    const uint boneIdx = ( *bonePos )->getBoneIndex();
    return m_skel.getPose( HandleArray::SpaceType::MODEL )[boneIdx];
}

void SkeletonComponent::setTransform( const Ra::Core::Utils::Index& roIdx,
                                      const Ra::Core::Transform& transform ) {
    CORE_ASSERT( canEdit( roIdx ), "Transform is not editable" );
    const auto& bonePos =
        std::find_if( m_boneDrawables.begin(), m_boneDrawables.end(), [roIdx]( const auto& bone ) {
            return bone->getRenderObjectIndex() == roIdx;
        } );

    const uint boneIdx = ( *bonePos )->getBoneIndex();
    const Ra::Core::Transform& TBoneModel =
        m_skel.getTransform( boneIdx, Ra::Core::Animation::HandleArray::SpaceType::MODEL );
    const Ra::Core::Transform& TBoneLocal =
        m_skel.getTransform( boneIdx, Ra::Core::Animation::HandleArray::SpaceType::LOCAL );
    auto diff = TBoneModel.inverse() * transform;
    m_skel.setTransform( boneIdx, TBoneLocal * diff,
                         Ra::Core::Animation::HandleArray::SpaceType::LOCAL );
}

// Build from fileData

void SkeletonComponent::handleSkeletonLoading( const Ra::Core::Asset::HandleData* data ) {
    m_skelName = data->getName();

    m_skel.setName( data->getName() );

    Ra::Core::Asset::createSkeleton( *data, m_skel );

    m_refPose = m_skel.getPose( HandleArray::SpaceType::LOCAL );

    setupSkeletonDisplay();
    setupIO();
}

void SkeletonComponent::handleAnimationLoading( const std::vector<Ra::Core::Asset::AnimationData*>& data ) {
    CORE_ASSERT( ( m_skel.size() != 0 ), "A Skeleton should be loaded first." );
    m_animations.clear();
    m_animations.reserve( data.size() );

    auto pose = m_skel.getPose( HandleArray::SpaceType::LOCAL );
    for ( uint n = 0; n < data.size(); ++n )
    {
        m_animations.emplace_back();
        m_animations.back().reserve( m_skel.size() );
        auto handleAnim = data[n]->getHandleData();
        for ( uint i = 0; i < m_skel.size(); ++i )
        {
            auto it = std::find_if( handleAnim.cbegin(), handleAnim.cend(),
                [this, i](const auto& ha){ return m_skel.getLabel( i ) == ha.m_name; } );
            if ( it == handleAnim.cend() )
            {
                m_animations.back().push_back( KeyFramedValue( pose[i], 0_ra ) );
            }
            else
            {
                m_animations.back().push_back( it->m_anim );
            }
        }

        m_dt.push_back( data[n]->getTimeStep() );
    }
    if ( m_animations.size() == 0 )
    {
        m_animations.emplace_back();
        for ( uint i = 0; i < m_skel.size(); ++i )
        {
            m_animations[0].push_back( KeyFramedValue( pose[i], 0_ra ) );
        }
    }
    m_animationID   = 0;
    m_animationTime = 0_ra;
}

// Skeleton-based animation data

void SkeletonComponent::setSkeleton( const Skeleton& skel ) {
    m_skel    = skel;
    m_refPose = skel.getPose( HandleArray::SpaceType::LOCAL );
    setupSkeletonDisplay();
}

SkeletonComponent::Animation& SkeletonComponent::addNewAnimation() {
    m_animations.emplace_back();
    for ( uint i = 0; i < m_skel.size(); ++i )
    {
        m_animations.back().push_back( KeyFramedValue( m_refPose[i], 0_ra ) );
    }
    return m_animations.back();
}

void SkeletonComponent::removeAnimation( const size_t i ) {
    CORE_ASSERT( i < m_animations.size(), "Out of bound index." );
    m_animations.erase( m_animations.begin() + i );
    m_animationID = i > 1 ? i - 1 : 0;
}

void SkeletonComponent::useAnimation( const size_t i ) {
    if ( i < m_animations.size() ) { m_animationID = i; }
}

size_t SkeletonComponent::getAnimationId() const {
    return m_animationID;
}

// Animation Process

void SkeletonComponent::update( Scalar t ) {
    m_wasReset = Ra::Core::Math::areApproxEqual( t, 0_ra );
    if ( m_wasReset )
    {
        m_animationTime = t;
        m_skel.setPose( m_refPose, HandleArray::SpaceType::LOCAL );

        updateDisplay();
        return;
    }

    m_animationTime = m_speed * t;
    Scalar lastTime = 0;
    for ( auto boneAnim : m_animations[m_animationID] )
    {
        lastTime = std::max( lastTime, *boneAnim.getTimes().rbegin() );
    }
    if ( m_autoRepeat )
    {
        if ( !m_pingPong ) { m_animationTime = std::fmod( m_animationTime, lastTime ); }
        else
        {
            m_animationTime = std::fmod( m_animationTime, 2 * lastTime );
            if ( m_animationTime > lastTime ) { m_animationTime = 2 * lastTime - m_animationTime; }
        }
    }
    else if ( m_pingPong )
    {
        if ( m_animationTime > 2 * lastTime ) { m_animationTime = 0_ra; }
        else if ( m_animationTime > lastTime )
        { m_animationTime = 2 * lastTime - m_animationTime; }
    }

    // get the current pose from the animation
    Ra::Core::Animation::Pose pose = m_skel.getPose( HandleArray::SpaceType::LOCAL );
    if ( !m_animations.empty() )
    {
#pragma omp parallel for
        for ( int i = 0; i < int( m_animations[m_animationID].size() ); ++i )
        {
            pose[uint( i )] = m_animations[m_animationID][uint( i )].at(
                m_animationTime, Ra::Core::Animation::linearInterpolate<Ra::Core::Transform> );
        }
    }
    else
    { pose = m_refPose; }
    m_skel.setPose( pose, HandleArray::SpaceType::LOCAL );

    updateDisplay();
}

Scalar SkeletonComponent::getAnimationTime() const {
    return m_animationTime;
}

Scalar SkeletonComponent::getAnimationDuration() const {
    if ( m_animations.empty() ) { return 0_ra; }
    Scalar startTime = std::numeric_limits<Scalar>::max();
    Scalar endTime   = 0;
    for ( auto boneAnim : m_animations[m_animationID] )
    {
        const auto& times = boneAnim.getTimes();
        startTime = std::min( startTime, *times.begin() );
        endTime   = std::max( endTime, *times.rbegin() );
    }
    return endTime - startTime;
}

void SkeletonComponent::toggleAnimationTimeStep( const bool status ) {
    m_animationTimeStep = status;
}

bool SkeletonComponent::usesAnimationTimeStep() const {
    return m_animationTimeStep;
}

void SkeletonComponent::setSpeed( const Scalar value ) {
    m_speed = value;
}

Scalar SkeletonComponent::getSpeed() const {
    return m_speed;
}

void SkeletonComponent::autoRepeat( const bool status ) {
    m_autoRepeat = status;
}

bool SkeletonComponent::isAutoRepeat() const {
    return m_autoRepeat;
}

void SkeletonComponent::pingPong( const bool status ) {
    m_pingPong = status;
}

bool SkeletonComponent::isPingPong() const {
    return m_pingPong;
}

// Caching frames

void SkeletonComponent::cacheFrame( const std::string& dir, uint frame ) const {
    std::ofstream file( dir + "/" + m_skelName + "_frame" + std::to_string( frame ) + ".anim",
                        std::ios::trunc | std::ios::out | std::ios::binary );
    if ( !file.is_open() ) { return; }
    file.write( reinterpret_cast<const char*>( &m_animationID ), sizeof m_animationID );
    file.write( reinterpret_cast<const char*>( &m_animationTimeStep ), sizeof m_animationTimeStep );
    file.write( reinterpret_cast<const char*>( &m_animationTime ), sizeof m_animationTime );
    file.write( reinterpret_cast<const char*>( &m_speed ), sizeof m_speed );
    file.write( reinterpret_cast<const char*>( &m_autoRepeat ), sizeof m_autoRepeat );
    file.write( reinterpret_cast<const char*>( &m_pingPong ), sizeof m_pingPong );
    const auto& pose = m_skel.getPose( HandleArray::SpaceType::LOCAL );
    file.write( reinterpret_cast<const char*>( pose.data() ),
                ( sizeof pose[0] ) * uint( pose.size() ) );
    LOG( logINFO ) << "Saving anim data at time: " << m_animationTime;
}

bool SkeletonComponent::restoreFrame( const std::string& dir, uint frame ) {
    std::ifstream file( dir + "/" + m_skelName + "_frame" + std::to_string( frame ) + ".anim",
                        std::ios::in | std::ios::binary );
    if ( !file.is_open() ) { return false; }
    if ( !file.read( reinterpret_cast<char*>( &m_animationID ), sizeof m_animationID ) )
    { return false; }
    if ( !file.read( reinterpret_cast<char*>( &m_animationTimeStep ), sizeof m_animationTimeStep ) )
    { return false; }
    if ( !file.read( reinterpret_cast<char*>( &m_animationTime ), sizeof m_animationTime ) )
    { return false; }
    if ( !file.read( reinterpret_cast<char*>( &m_speed ), sizeof m_speed ) ) { return false; }
    if ( !file.read( reinterpret_cast<char*>( &m_autoRepeat ), sizeof m_autoRepeat ) )
    { return false; }
    if ( !file.read( reinterpret_cast<char*>( &m_pingPong ), sizeof m_pingPong ) ) { return false; }
    auto pose = m_skel.getPose( HandleArray::SpaceType::LOCAL );
    if ( !file.read( reinterpret_cast<char*>( pose.data() ),
                     ( sizeof pose[0] ) * uint( pose.size() ) ) )
    { return false; }
    m_skel.setPose( pose, HandleArray::SpaceType::LOCAL );

    // update the render objects
    updateDisplay();

    return true;
}

// Skeleton display

void SkeletonComponent::setXray( bool on ) const {
    for ( const auto& b : m_boneDrawables )
    {
        b->setXray( on );
    }
}

bool SkeletonComponent::isXray() const {
    if ( m_boneDrawables.size() == 0 ) return false;
    return m_boneDrawables.front()->isXray();
}

void SkeletonComponent::toggleSkeleton( const bool status ) {
    for ( const auto& b : m_boneDrawables )
    {
        b->setVisible( status );
    }
}

bool SkeletonComponent::isShowingSkeleton() const {
    if ( m_boneDrawables.size() == 0 ) return false;
    return m_boneDrawables.front()->isVisible();
}

void SkeletonComponent::setupSkeletonDisplay() {
    m_renderObjects.clear();
    m_boneDrawables.clear();
    for ( uint i = 0; i < m_skel.size(); ++i )
    {
        if ( !m_skel.m_graph.isLeaf( i ) && !m_skel.m_graph.isRoot( i ) &&
             m_skel.getLabel( i ).find( "_$AssimpFbx$_" ) == std::string::npos )
        {
            std::string name = m_skel.getLabel( i ) + "_" + std::to_string( i );
            m_boneDrawables.emplace_back( new SkeletonBoneRenderObject( name, this, i ) );
            m_boneMap[m_renderObjects.back()] = i;
        }
        else
        { LOG( logDEBUG ) << "Bone " << m_skel.getLabel( i ) << " not displayed."; }
    }
}

void SkeletonComponent::printSkeleton( const Skeleton& skeleton ) {
    std::deque<uint> queue;
    std::deque<int> levels;

    queue.push_back( 0 );
    levels.push_back( 0 );
    while ( !queue.empty() )
    {
        uint i = queue.front();
        queue.pop_front();
        int level = levels.front();
        levels.pop_front();
        std::cout << i << " " << skeleton.getLabel( i ) << "\t";
        for ( const auto& c : skeleton.m_graph.children()[i] )
        {
            queue.push_back( c );
            levels.push_back( level + 1 );
        }

        if ( levels.front() != level ) { std::cout << std::endl; }
    }
}

void SkeletonComponent::updateDisplay() {
    for ( auto& bone : m_boneDrawables )
    {
        bone->update();
    }
}

// Component Communication (CC)

void SkeletonComponent::setupIO() {
    ComponentMessenger::CallbackTypes<Skeleton>::Getter skelOut =
        std::bind( &SkeletonComponent::getSkeletonOutput, this );
    ComponentMessenger::getInstance()->registerOutput<Skeleton>( getEntity(), this, m_skelName, skelOut );

    using BoneMap = std::map<Ra::Core::Utils::Index, uint>;
    ComponentMessenger::CallbackTypes<BoneMap>::Getter boneMapOut =
        std::bind( &SkeletonComponent::getBoneRO2idx, this );
    ComponentMessenger::getInstance()->registerOutput<BoneMap>( getEntity(), this, m_skelName, boneMapOut );

    ComponentMessenger::CallbackTypes<Ra::Core::Animation::RefPose>::Getter refpOut =
        std::bind( &SkeletonComponent::getRefPoseOutput, this );
    ComponentMessenger::getInstance()->registerOutput<Ra::Core::Animation::Pose>(
        getEntity(), this, m_skelName, refpOut );

    ComponentMessenger::CallbackTypes<Animation>::Getter animOut =
        std::bind( &SkeletonComponent::getAnimationOutput, this );
    ComponentMessenger::getInstance()->registerOutput<Animation>( getEntity(), this, m_skelName, animOut );

    ComponentMessenger::CallbackTypes<Scalar>::Getter timeOut =
        std::bind( &SkeletonComponent::getTimeOutput, this );
    ComponentMessenger::getInstance()->registerOutput<Scalar>( getEntity(), this, m_skelName, timeOut );

    ComponentMessenger::CallbackTypes<bool>::Getter resetOut =
        std::bind( &SkeletonComponent::getWasReset, this );
    ComponentMessenger::getInstance()->registerOutput<bool>( getEntity(), this, m_skelName, resetOut );
}

const Ra::Core::Animation::Skeleton* SkeletonComponent::getSkeletonOutput() const {
    return &m_skel;
}

const std::map<Ra::Core::Utils::Index, uint>* SkeletonComponent::getBoneRO2idx() const {
    return &m_boneMap;
}

const Ra::Core::Animation::RefPose* SkeletonComponent::getRefPoseOutput() const {
    return &m_refPose;
}

const SkeletonComponent::Animation* SkeletonComponent::getAnimationOutput() const {
    if ( m_animations.empty() ) { return nullptr; }
    return &m_animations[m_animationID];
}

const Scalar* SkeletonComponent::getTimeOutput() const {
    return &m_animationTime;
}

const bool* SkeletonComponent::getWasReset() const {
    return &m_wasReset;
}

} // namespace SkeletonBasedAnimationPlugin
