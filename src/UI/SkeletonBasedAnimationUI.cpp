#include "SkeletonBasedAnimationUI.h"
#include "ui_SkeletonBasedAnimationUI.h"


#include <QFileDialog>
#include <QSettings>

#include <fstream>
#include <iostream>

#include <Core/CoreMacros.hpp>
#include <Core/Animation/KeyFramedValueInterpolators.hpp>

#include <SkeletonComponent.hpp>
#include <SkinningComponent.hpp>

SkeletonBasedAnimationUI::SkeletonBasedAnimationUI( QWidget* parent ) :
    QFrame( parent ),
    ui( new Ui::SkeletonBasedAnimationUI ) {
    ui->setupUi( this );
    connect( ui->actionXray, &QAction::toggled, this, &SkeletonBasedAnimationUI::on_m_xray_clicked );
    connect( ui->actionXray, &QAction::toggled, this, &SkeletonBasedAnimationUI::on_m_showSkeleton_toggled );
    connect(
        ui->actionLBS, &QAction::triggered, this, &SkeletonBasedAnimationUI::onLSBActionTriggered );
    connect(
        ui->actionDQS, &QAction::triggered, this, &SkeletonBasedAnimationUI::onDQSActionTriggered );
    connect(
        ui->actionCoR, &QAction::triggered, this, &SkeletonBasedAnimationUI::onCoRActionTriggered );
    connect( ui->actionSTBSLBS,
             &QAction::triggered,
             this,
             &SkeletonBasedAnimationUI::onSTBSLBSActionTriggered );
    connect( ui->actionSTBSDQS,
             &QAction::triggered,
             this,
             &SkeletonBasedAnimationUI::onSTBSDQSActionTriggered );
}

SkeletonBasedAnimationUI::~SkeletonBasedAnimationUI() {
    delete ui;
}

void SkeletonBasedAnimationUI::setCurrentSkel( const Ra::Engine::ItemEntry& entry,
                                               SkeletonBasedAnimationPlugin::SkeletonComponent* skel ) {
    m_currentSkeleton = skel;
    if ( !ui->m_applyToAll->isChecked() && m_currentSkeleton == nullptr )
    {
        ui->tabWidget->setEnabled( false );
        return;
    }
    ui->tabWidget->setEnabled( true );

    if ( m_currentSkeleton == nullptr ) { return; }
    ui->actionXray->setChecked( m_currentSkeleton->isXray() );
    ui->m_speed->setValue( double( m_currentSkeleton->getSpeed() ) );
    ui->m_autoRepeat->setChecked( m_currentSkeleton->isAutoRepeat() );
    ui->m_pingPong->setChecked( m_currentSkeleton->isPingPong() );
    ui->m_xray->setChecked( m_currentSkeleton->isXray() );
    ui->m_showSkeleton->setChecked( m_currentSkeleton->isShowingSkeleton() );
    ui->m_currentAnimation->blockSignals( true );
    ui->m_currentAnimation->clear();
    for ( size_t i = 0; i < m_currentSkeleton->getAnimationCount(); ++i )
    {
        ui->m_currentAnimation->addItem( "#" + QString::number( i ) );
    }
    ui->m_currentAnimation->setCurrentIndex( int( m_currentSkeleton->getAnimationId() ) );
    ui->m_currentAnimation->blockSignals( false );
}

void SkeletonBasedAnimationUI::setCurrentSkin( const Ra::Engine::ItemEntry& entry,
                                               SkeletonBasedAnimationPlugin::SkinningComponent* skin ) {
    m_currentSkinning = skin;
    if ( m_currentSkinning )
    {
        ui->m_skinningMethod->setEnabled( true );
        ui->m_showWeights->setEnabled( true );
        ui->m_weightsType->setEnabled( true );
        ui->actionLBS->setEnabled( true );
        ui->actionDQS->setEnabled( true );
        ui->actionCoR->setEnabled( true );
        ui->actionSTBSLBS->setEnabled( true );
        ui->actionSTBSDQS->setEnabled( true );
        ui->m_skinningMethod->setCurrentIndex( int( m_currentSkinning->getSkinningType() ) );
        on_m_skinningMethod_currentIndexChanged( int( m_currentSkinning->getSkinningType() ) );
        ui->m_smartStretch->setEnabled( true );
        ui->m_smartStretch->setChecked( m_currentSkinning->getSmartStretch() );
    }
    else
    {
        ui->m_skinningMethod->setEnabled( false );
        ui->m_showWeights->setEnabled( false );
        ui->m_weightsType->setEnabled( false );
        ui->actionLBS->setEnabled( false );
        ui->actionDQS->setEnabled( false );
        ui->actionCoR->setEnabled( false );
        ui->actionSTBSLBS->setEnabled( false );
        ui->actionSTBSDQS->setEnabled( false );
        ui->m_smartStretch->setEnabled( false );
    }
    askForUpdate();
}

void SkeletonBasedAnimationUI::updateTime( float t ) {
    ui->m_animTimeDisplay->setText( QString::number( double( t ) ) );
}

void SkeletonBasedAnimationUI::on_actionXray_triggered( bool checked ) {
    ui->m_xray->setChecked( checked );
    if ( m_currentSkeleton || ui->m_applyToAll->isChecked() ) { on_m_xray_clicked( checked ); }
    askForUpdate();
}

void SkeletonBasedAnimationUI::onLSBActionTriggered() {
    ui->m_skinningMethod->setCurrentIndex( 0 );
    ui->actionLBS->setChecked( true );
    ui->actionDQS->setChecked( false );
    ui->actionCoR->setChecked( false );
    ui->actionSTBSLBS->setChecked( false );
    ui->actionSTBSDQS->setChecked( false );
    askForUpdate();
}

void SkeletonBasedAnimationUI::onDQSActionTriggered() {
    ui->m_skinningMethod->setCurrentIndex( 1 );
    ui->actionLBS->setChecked( false );
    ui->actionDQS->setChecked( true );
    ui->actionCoR->setChecked( false );
    ui->actionSTBSLBS->setChecked( false );
    ui->actionSTBSDQS->setChecked( false );
    askForUpdate();
}

void SkeletonBasedAnimationUI::onCoRActionTriggered() {
    ui->m_skinningMethod->setCurrentIndex( 2 );
    ui->actionLBS->setChecked( false );
    ui->actionDQS->setChecked( false );
    ui->actionCoR->setChecked( true );
    ui->actionSTBSLBS->setChecked( false );
    ui->actionSTBSDQS->setChecked( false );
    askForUpdate();
}

void SkeletonBasedAnimationUI::onSTBSLBSActionTriggered() {
    ui->m_skinningMethod->setCurrentIndex( 3 );
    ui->actionLBS->setChecked( false );
    ui->actionDQS->setChecked( false );
    ui->actionCoR->setChecked( false );
    ui->actionSTBSLBS->setChecked( true );
    ui->actionSTBSDQS->setChecked( false );
    askForUpdate();
}

void SkeletonBasedAnimationUI::onSTBSDQSActionTriggered() {
    ui->m_skinningMethod->setCurrentIndex( 4 );
    ui->actionLBS->setChecked( false );
    ui->actionDQS->setChecked( false );
    ui->actionCoR->setChecked( false );
    ui->actionSTBSLBS->setChecked( false );
    ui->actionSTBSDQS->setChecked( true );
    askForUpdate();
}

void SkeletonBasedAnimationUI::on_m_smartStretch_toggled( bool checked )
{
    m_currentSkinning->setSmartStretch( checked );
    askForUpdate();
}

void SkeletonBasedAnimationUI::on_m_skinningMethod_currentIndexChanged( int newType ) {
    CORE_ASSERT( m_currentSkinning, "should be disabled" );
    CORE_ASSERT( newType >= 0 && newType < 5, "Invalid Skinning Type" );
    m_currentSkinning->setSkinningType(
        SkeletonBasedAnimationPlugin::SkinningComponent::SkinningType( newType ) );
    switch ( newType )
    {
    case 0:
    {
        ui->actionLBS->setChecked( true );
        ui->actionDQS->setChecked( false );
        ui->actionCoR->setChecked( false );
        ui->actionSTBSLBS->setChecked( false );
        ui->actionSTBSDQS->setChecked( false );
        ui->actionLBS->setIcon( QIcon( ":/Resources/Icons/LB_on.png" ) );
        ui->actionDQS->setIcon( QIcon( ":/Resources/Icons/DQ.png" ) );
        ui->actionCoR->setIcon( QIcon( ":/Resources/Icons/CoR.png" ) );
        ui->actionSTBSLBS->setIcon( QIcon( ":/Resources/Icons/STBSLB.png" ) );
        ui->actionSTBSDQS->setIcon( QIcon( ":/Resources/Icons/STBSDQ.png" ) );
        break;
    }
    case 1:
    {
        ui->actionLBS->setChecked( false );
        ui->actionDQS->setChecked( true );
        ui->actionCoR->setChecked( false );
        ui->actionSTBSLBS->setChecked( false );
        ui->actionSTBSDQS->setChecked( false );
        ui->actionLBS->setIcon( QIcon( ":/Resources/Icons/LB.png" ) );
        ui->actionDQS->setIcon( QIcon( ":/Resources/Icons/DQ_on.png" ) );
        ui->actionCoR->setIcon( QIcon( ":/Resources/Icons/CoR.png" ) );
        ui->actionSTBSLBS->setIcon( QIcon( ":/Resources/Icons/STBSLB.png" ) );
        ui->actionSTBSDQS->setIcon( QIcon( ":/Resources/Icons/STBSDQ.png" ) );
        break;
    }
    case 2:
    {
        ui->actionLBS->setChecked( false );
        ui->actionDQS->setChecked( false );
        ui->actionCoR->setChecked( true );
        ui->actionSTBSLBS->setChecked( false );
        ui->actionSTBSDQS->setChecked( false );
        ui->actionLBS->setIcon( QIcon( ":/Resources/Icons/LB.png" ) );
        ui->actionDQS->setIcon( QIcon( ":/Resources/Icons/DQ.png" ) );
        ui->actionCoR->setIcon( QIcon( ":/Resources/Icons/CoR_on.png" ) );
        ui->actionSTBSLBS->setIcon( QIcon( ":/Resources/Icons/STBSLB.png" ) );
        ui->actionSTBSDQS->setIcon( QIcon( ":/Resources/Icons/STBSDQ.png" ) );
        break;
    }
    case 3:
    {
        ui->actionLBS->setChecked( false );
        ui->actionDQS->setChecked( false );
        ui->actionCoR->setChecked( false );
        ui->actionSTBSLBS->setChecked( true );
        ui->actionSTBSDQS->setChecked( false );
        ui->actionLBS->setIcon( QIcon( ":/Resources/Icons/LB.png" ) );
        ui->actionDQS->setIcon( QIcon( ":/Resources/Icons/DQ.png" ) );
        ui->actionCoR->setIcon( QIcon( ":/Resources/Icons/CoR.png" ) );
        ui->actionSTBSLBS->setIcon( QIcon( ":/Resources/Icons/STBSLB_on.png" ) );
        ui->actionSTBSDQS->setIcon( QIcon( ":/Resources/Icons/STBSDQ.png" ) );
        break;
    }
    case 4:
    {
        ui->actionLBS->setChecked( false );
        ui->actionDQS->setChecked( false );
        ui->actionCoR->setChecked( false );
        ui->actionSTBSLBS->setChecked( false );
        ui->actionSTBSDQS->setChecked( true );
        ui->actionLBS->setIcon( QIcon( ":/Resources/Icons/LB.png" ) );
        ui->actionDQS->setIcon( QIcon( ":/Resources/Icons/DQ.png" ) );
        ui->actionCoR->setIcon( QIcon( ":/Resources/Icons/CoR.png" ) );
        ui->actionSTBSLBS->setIcon( QIcon( ":/Resources/Icons/STBSLB.png" ) );
        ui->actionSTBSDQS->setIcon( QIcon( ":/Resources/Icons/STBSDQ_on.png" ) );
        break;
    }
    default:
    { break; }
    }
    askForUpdate();
}

void SkeletonBasedAnimationUI::on_m_speed_valueChanged( double arg1 ) {
    if ( ui->m_applyToAll->isChecked() ) { emit animationSpeedAll( arg1 ); }
    else if ( m_currentSkeleton != nullptr )
    { m_currentSkeleton->setSpeed( Scalar( arg1 ) ); }
    askForUpdate();
}

void SkeletonBasedAnimationUI::on_m_autoRepeat_toggled( bool checked ) {
    if ( ui->m_applyToAll->isChecked() ) { emit autoRepeatAll( checked ); }
    else if ( m_currentSkeleton != nullptr )
    { m_currentSkeleton->autoRepeat( checked ); }
    askForUpdate();
}

void SkeletonBasedAnimationUI::on_m_currentAnimation_currentIndexChanged( int index ) {
    emit useAnim( m_currentSkeleton, index );
    askForUpdate();
}

void SkeletonBasedAnimationUI::on_m_newAnim_clicked() {
    const int num = ui->m_currentAnimation->count();
    ui->m_currentAnimation->blockSignals( true );
    ui->m_currentAnimation->addItem( "#" + QString::number( num ) );
    ui->m_currentAnimation->setCurrentIndex( num );
    ui->m_currentAnimation->blockSignals( false );
    m_currentSkeleton->addNewAnimation();
    emit useAnim( m_currentSkeleton, num );
    askForUpdate();
}

void SkeletonBasedAnimationUI::on_m_removeAnim_clicked() {
    if ( ui->m_currentAnimation->count() == 1 ) { return; }

    const int removeIndex = ui->m_currentAnimation->currentIndex();
    ui->m_currentAnimation->blockSignals( true );
    ui->m_currentAnimation->removeItem( removeIndex );
    m_currentSkeleton->removeAnimation( size_t( removeIndex ) );
    ui->m_currentAnimation->setCurrentIndex( int( m_currentSkeleton->getAnimationId() ) );
    ui->m_currentAnimation->blockSignals( false );
    emit useAnim( m_currentSkeleton, int( m_currentSkeleton->getAnimationId() ) );
    askForUpdate();
}

void SkeletonBasedAnimationUI::on_m_loadAnim_clicked() {
    QSettings settings;
    QString path = settings.value( "Animation::path", QDir::homePath() ).toString();
    path         = QFileDialog::getOpenFileName( nullptr, "Load Animation", path, "*.rdma" );
    if ( path.size() == 0 ) { return; }
    settings.setValue( "Animation::path", path );
    ui->m_animFile->setText( path.split( '/' ).last() );

    const int num = ui->m_currentAnimation->count();
    ui->m_currentAnimation->blockSignals( true );
    ui->m_currentAnimation->addItem( "#" + QString::number( num ) );
    ui->m_currentAnimation->setCurrentIndex( num );
    ui->m_currentAnimation->blockSignals( false );

    auto& anim = m_currentSkeleton->addNewAnimation();
    std::ifstream file( path.toStdString(), std::ios::binary );
    // Todo: deal with anim timestep when used
    size_t n;
    Scalar t;
    Ra::Core::Transform T;
    for ( auto& bAnim : anim )
    {
        file.read( reinterpret_cast<char*>( &n ), sizeof( n ) );
        for ( size_t i = 0; i < n; ++i )
        {
            file.read( reinterpret_cast<char*>( &t ), sizeof( t ) );
            file.read( reinterpret_cast<char*>( &T ), sizeof( T ) );
            bAnim.insertKeyFrame( t, T );
        }
    }
    emit useAnim( m_currentSkeleton, num );
    askForUpdate();
}

void SkeletonBasedAnimationUI::on_m_saveAnim_clicked() {
    QSettings settings;
    QString path = settings.value( "Animation::path", QDir::homePath() ).toString();
    path         = QFileDialog::getSaveFileName( nullptr, "Load Animation", path, "*.rdma" );
    if ( path.size() == 0 ) { return; }
    settings.setValue( "Animation::path", path );
    ui->m_animFile->setText( path.split( '/' ).last() );

    const auto& anim = m_currentSkeleton->getAnimation( m_currentSkeleton->getAnimationId() );
    std::ofstream file( path.toStdString(), std::ios::trunc | std::ios::binary );
    // Todo: deal with anim timestep when used
    size_t n;
    for ( const auto& bAnim : anim )
    {
        n = bAnim.size();
        file.write( reinterpret_cast<const char*>( &n ), sizeof( n ) );
        for ( const auto t : bAnim.getTimes() )
        {
            file.write( reinterpret_cast<const char*>( &t ), sizeof( t ) );
            auto kf = bAnim.at( t, Ra::Core::Animation::linearInterpolate<Ra::Core::Transform> );
            file.write( reinterpret_cast<const char*>( &kf ), sizeof( kf ) );
        }
    }
}

void SkeletonBasedAnimationUI::on_m_pingPong_toggled( bool checked ) {
    if ( ui->m_applyToAll->isChecked() ) { emit pingPongAll( checked ); }
    else if ( m_currentSkeleton != nullptr )
    { m_currentSkeleton->pingPong( checked ); }
    askForUpdate();
}

void SkeletonBasedAnimationUI::on_m_xray_clicked( bool checked ) {
    if ( ui->m_applyToAll->isChecked() ) { emit toggleXrayAll( checked ); }
    else if ( m_currentSkeleton != nullptr )
    { m_currentSkeleton->setXray( checked ); }
    askForUpdate();
}

void SkeletonBasedAnimationUI::on_m_showSkeleton_toggled( bool checked ) {
    if ( ui->m_applyToAll->isChecked() ) { emit showSkeletonAll( checked ); }
    else if ( m_currentSkeleton != nullptr )
    { m_currentSkeleton->toggleSkeleton( checked ); }
    askForUpdate();
}

void SkeletonBasedAnimationUI::on_m_showWeights_toggled( bool checked ) {
    emit showWeights( checked );
}

void SkeletonBasedAnimationUI::on_m_weightsType_currentIndexChanged( int newType ) {
    emit showWeightsType( newType );
}
