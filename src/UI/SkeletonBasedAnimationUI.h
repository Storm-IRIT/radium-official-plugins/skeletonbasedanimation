#ifndef ANIMATIONUI_H
#define ANIMATIONUI_H

#include <QFrame>

namespace Ra {
namespace Engine {
class RadiumEngine;
struct ItemEntry;
} // namespace Engine
namespace Guibase {
class SelectionManager;
}
} // namespace Ra

namespace Ui {
class SkeletonBasedAnimationUI;
}

namespace SkeletonBasedAnimationPlugin {
class SkeletonBasedAnimationPluginC;
class SkeletonComponent;
class SkinningComponent;
class SkinningSystem;
} // namespace SkeletonBasedAnimationPlugin

class SkeletonBasedAnimationUI : public QFrame
{
    Q_OBJECT

    friend class SkeletonBasedAnimationPlugin::SkeletonBasedAnimationPluginC;

  public:
    explicit SkeletonBasedAnimationUI( QWidget* parent = nullptr );
    ~SkeletonBasedAnimationUI();

    void updateTime( float t );

  signals:
    void askForUpdate();

    void animationSpeedAll( double );
    void autoRepeatAll( bool );
    void pingPongAll( bool );

    void useAnim( SkeletonBasedAnimationPlugin::SkeletonComponent*, int );

    void toggleXrayAll( bool );
    void showSkeletonAll( bool );

    void showWeights( bool );
    void showWeightsType( int );

  public slots:
    void setCurrentSkel( const Ra::Engine::ItemEntry& entry,
                         SkeletonBasedAnimationPlugin::SkeletonComponent* skel );
    void setCurrentSkin( const Ra::Engine::ItemEntry& entry,
                         SkeletonBasedAnimationPlugin::SkinningComponent* skin );

  private slots:
    void on_actionXray_triggered( bool checked );

    void on_m_speed_valueChanged( double arg1 );
    void on_m_pingPong_toggled( bool checked );
    void on_m_autoRepeat_toggled( bool checked );

    void on_m_currentAnimation_currentIndexChanged( int index );
    void on_m_newAnim_clicked();
    void on_m_removeAnim_clicked();
    void on_m_loadAnim_clicked();
    void on_m_saveAnim_clicked();

    void on_m_xray_clicked( bool checked );
    void on_m_showSkeleton_toggled( bool checked );

    void on_m_smartStretch_toggled( bool checked );
    void on_m_skinningMethod_currentIndexChanged( int index );
    void on_m_showWeights_toggled( bool checked );
    void on_m_weightsType_currentIndexChanged( int index );
    void onLSBActionTriggered();
    void onDQSActionTriggered();
    void onCoRActionTriggered();
    void onSTBSLBSActionTriggered();
    void onSTBSDQSActionTriggered();

private:
    void unpolishPlayButton();

  private:
    Ui::SkeletonBasedAnimationUI* ui;
    SkeletonBasedAnimationPlugin::SkinningComponent* m_currentSkinning{nullptr};
    SkeletonBasedAnimationPlugin::SkeletonComponent* m_currentSkeleton{nullptr};
};

#endif // ANIMATIONUI_H
