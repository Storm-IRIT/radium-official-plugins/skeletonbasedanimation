#include <SkeletonBasedAnimationPlugin.hpp>

#include <QAction>
#include <QFileDialog>
#include <QIcon>
#include <QSettings>
#include <QToolBar>

#include <Core/Utils/Log.hpp>

#include <Engine/Managers/SignalManager/SignalManager.hpp>
#include <Engine/RadiumEngine.hpp>
#include <Engine/Renderer/Texture/TextureManager.hpp>
#include <GuiBase/SelectionManager/SelectionManager.hpp>

#include "ui_SkeletonBasedAnimationUI.h"
#include <UI/SkeletonBasedAnimationUI.h>

#include <SkeletonBasedAnimationSystem.hpp>
#include <SkeletonComponent.hpp>
#include <SkinningComponent.hpp>

using namespace Ra::Core::Utils; // log

namespace SkeletonBasedAnimationPlugin {

SkeletonBasedAnimationPluginC::SkeletonBasedAnimationPluginC() = default;

SkeletonBasedAnimationPluginC::~SkeletonBasedAnimationPluginC() = default;

void SkeletonBasedAnimationPluginC::registerPlugin( const Ra::Plugins::Context& context ) {
    QSettings settings;
    QString path = settings.value( "AnimDataDir" ).toString();
    if ( path.isEmpty() ) { path = QString( context.m_exportDir.c_str() ); }

    // try to link to the TimeSystem
    m_system = new SkeletonBasedAnimationSystem;
    context.m_engine->registerSystem( "SkeletonAnimationSystem", m_system );
    m_system->setTimeline( context.m_timeline );

    // register callback for end of frame
    context.m_engine->getSignalManager()->m_frameEndCallbacks.push_back(
        std::bind( &SkeletonBasedAnimationPluginC::updateAnimTime, this ) );

    // connect to selection manager
    m_selectionManager = context.m_selectionManager;
    if ( m_selectionManager )
    {
        connect( m_selectionManager,
                 &Ra::GuiBase::SelectionManager::currentChanged,
                 this,
                 &SkeletonBasedAnimationPluginC::onCurrentChanged );
    }

    connect( this,
             &SkeletonBasedAnimationPluginC::askForUpdate,
             &context,
             &Ra::Plugins::Context::askForUpdate );
    connect( this,
             &SkeletonBasedAnimationPluginC::setContinuousUpdate,
             &context,
             &Ra::Plugins::Context::setContinuousUpdate );
}

bool SkeletonBasedAnimationPluginC::doAddWidget( QString& name ) {
    name = "SkeletonBasedAnimation";
    return true;
}

QWidget* SkeletonBasedAnimationPluginC::getWidget() {
    m_widget = new SkeletonBasedAnimationUI();
    connect( m_widget, &SkeletonBasedAnimationUI::toggleXrayAll, this, &SkeletonBasedAnimationPluginC::toggleXrayAll );
    connect( m_widget, &SkeletonBasedAnimationUI::showSkeletonAll, this, &SkeletonBasedAnimationPluginC::toggleSkeletonAll );
    connect(
        m_widget, &SkeletonBasedAnimationUI::animationSpeedAll, this, &SkeletonBasedAnimationPluginC::setAnimationSpeedAll );
    connect( m_widget, &SkeletonBasedAnimationUI::autoRepeatAll, this, &SkeletonBasedAnimationPluginC::autoRepeatAll );
    connect( m_widget, &SkeletonBasedAnimationUI::pingPongAll, this, &SkeletonBasedAnimationPluginC::pingPongAll );
    connect( m_widget, &SkeletonBasedAnimationUI::askForUpdate, this, &SkeletonBasedAnimationPluginC::askForUpdate );
    connect( m_widget, &SkeletonBasedAnimationUI::useAnim, this, &SkeletonBasedAnimationPluginC::useAnim );
    connect( m_widget,
             &SkeletonBasedAnimationUI::showWeights,
             this,
             &SkeletonBasedAnimationPluginC::onShowWeights );
    connect( m_widget,
             &SkeletonBasedAnimationUI::showWeightsType,
             this,
             &SkeletonBasedAnimationPluginC::onShowWeightsType );
    connect( m_widget,
             &SkeletonBasedAnimationUI::askForUpdate,
             this,
             &SkeletonBasedAnimationPluginC::askForUpdate );

    return m_widget;
}

bool SkeletonBasedAnimationPluginC::doAddMenu() {
    return false;
}

QMenu* SkeletonBasedAnimationPluginC::getMenu() {
    return nullptr;
}

bool SkeletonBasedAnimationPluginC::doAddAction( int& nb ) {
    nb = 9;
    return true;
}

QAction* SkeletonBasedAnimationPluginC::getAction( int id ) {
    switch ( id )
    {
    case 0:
        return m_widget->ui->actionXray;
    case 4:
        return m_widget->ui->actionLBS;
    case 5:
        return m_widget->ui->actionDQS;
    case 6:
        return m_widget->ui->actionCoR;
    case 7:
        return m_widget->ui->actionSTBSLBS;
    case 8:
        return m_widget->ui->actionSTBSDQS;
    default:
        return nullptr;
    }
}

bool SkeletonBasedAnimationPluginC::doAddROpenGLInitializer() {
    return m_system != nullptr;
}

void SkeletonBasedAnimationPluginC::openGlInitialize( const Ra::Plugins::Context& context ) {
    if ( !m_system ) { return; }
    QImage influenceImage( ":/Resources/Textures/Influence0.png" );
    auto img = influenceImage.convertToFormat( QImage::Format_RGB888 );
    Ra::Engine::TextureParameters texData;
    texData.wrapS     = gl::GL_CLAMP_TO_EDGE;
    texData.wrapT     = gl::GL_CLAMP_TO_EDGE;
    texData.minFilter = gl::GL_NEAREST;
    texData.magFilter = gl::GL_NEAREST;
    texData.width     = size_t( img.width() );
    texData.height    = size_t( img.height() );
    texData.format    = gl::GL_RGB;
    texData.texels    = img.bits();
    texData.name      = ":/Resources/Textures/Influence0.png";
    context.m_engine->getTextureManager()->getOrLoadTexture( texData );
}

void SkeletonBasedAnimationPluginC::onCurrentChanged( const QModelIndex& /*current*/,
                                                      const QModelIndex& /*prev*/ ) {
    Ra::Engine::ItemEntry it = m_selectionManager->currentItem();
    m_widget->setCurrentSkin( it, nullptr );
    m_widget->setCurrentSkel( it, nullptr );
    if ( it.m_entity )
    {
        auto comps = m_system->getEntityComponents( it.m_entity );
        if ( comps.size() != 0 )
        {
            for ( auto& comp : comps )
            {
                if ( comp->getName().compare( 0, 3, "AC_" ) == 0 ) {
                    m_widget->setCurrentSkel( it, static_cast<SkeletonComponent*>( comp ) );
                }
                if ( comp->getName().compare( 0, 4, "SkC_" ) == 0 ) {
                    auto skin = static_cast<SkinningComponent*>( comp );
                    m_widget->setCurrentSkin( it, skin );
                    // deal with bone selection for weights display
                    using BoneMap = std::map<Ra::Core::Utils::Index, uint>;
                    auto CM       = Ra::Engine::ComponentMessenger::getInstance();
                    auto BM       = *CM->getterCallback<BoneMap>( it.m_entity, skin->m_skelName )();
                    auto b_it     = BM.find( it.m_roIndex );
                    if ( b_it != BM.end() ) { skin->setWeightBone( b_it->second ); }
                }
            }
        }
    }
    askForUpdate();
}

void SkeletonBasedAnimationPluginC::toggleXrayAll( bool on ) {
    CORE_ASSERT( m_system, "System should be there " );
    m_system->setXray( on );
    askForUpdate();
}

void SkeletonBasedAnimationPluginC::toggleSkeletonAll( bool status ) {
    m_system->toggleSkeleton( status );
    askForUpdate();
}

void SkeletonBasedAnimationPluginC::setAnimationSpeedAll( Scalar value ) {
    m_system->setAnimationSpeed( value );
}

void SkeletonBasedAnimationPluginC::autoRepeatAll( bool status ) {
    m_system->autoRepeat( status );
}

void SkeletonBasedAnimationPluginC::pingPongAll( bool status ) {
    m_system->pingPong( status );
}

void SkeletonBasedAnimationPluginC::updateAnimTime() {
    m_widget->updateTime( m_system->getAnimationTime( m_selectionManager->currentItem() ) );
    askForUpdate();
}

void SkeletonBasedAnimationPluginC::useAnim( SkeletonComponent* comp, int id ) {
    m_system->useAnim( comp, id );
}

void SkeletonBasedAnimationPluginC::onShowWeights( bool on ) {
    m_system->showWeights( on );
    askForUpdate();
}

void SkeletonBasedAnimationPluginC::onShowWeightsType( int type ) {
    m_system->showWeightsType( type );
    askForUpdate();
}

} // namespace SkeletonBasedAnimationPlugin
