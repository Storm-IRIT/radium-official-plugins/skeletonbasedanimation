#ifndef SKELETONBASEDANIMATIONPLUGIN_PLUGIN_HPP_
#define SKELETONBASEDANIMATIONPLUGIN_PLUGIN_HPP_

#include <Core/CoreMacros.hpp>
#include <PluginBase/RadiumPluginInterface.hpp>
#include <QAction>
#include <QObject>
#include <QtPlugin>

#include <UI/SkeletonBasedAnimationUI.h>

#include <SkeletonBasedAnimationPluginMacros.hpp>

namespace Ra {
namespace Engine {
class RadiumEngine;
} // namespace Engine
} // namespace Ra

/// The SkeletonBasedAnimationPlugin manages skeleton-based character animation.
namespace SkeletonBasedAnimationPlugin {
// Due to an ambigous name while compiling with Clang, we must differentiate the
// plugin class from plugin namespace
class SkeletonBasedAnimationPluginC : public QObject, Ra::Plugins::RadiumPluginInterface
{
    Q_OBJECT
    Q_RADIUM_PLUGIN_METADATA
    Q_INTERFACES( Ra::Plugins::RadiumPluginInterface )

  public:
    SkeletonBasedAnimationPluginC();
    ~SkeletonBasedAnimationPluginC();

    void registerPlugin( const Ra::Plugins::Context& context ) override;

    bool doAddWidget( QString& name ) override;
    QWidget* getWidget() override;

    bool doAddMenu() override;
    QMenu* getMenu() override;

    bool doAddAction( int& nb ) override;
    QAction* getAction( int id ) override;

    bool doAddROpenGLInitializer() override;
    void openGlInitialize( const Ra::Plugins::Context& context ) override;

  public slots:
    /**
     * Slot for the user activating xray display of bones.
     */
    void toggleXrayAll( bool on );

    /**
     * Slot for the user activating display of bones.
     */
    void toggleSkeletonAll( bool on );

    /**
     * Slot for the user changing the animation speed.
     */
    void setAnimationSpeedAll( Scalar value );

    /**
     * Slot for the user asking to use animation auto repeat.
     */
    void autoRepeatAll( bool status );

    /**
     * Slot for the user asking to use animation ping-pong.
     */
    void pingPongAll( bool status );

    /**
     * Updates the displayed animation time.
     */
    void updateAnimTime();

    /**
     * Updates the plugin's widget w.r.t. the new selection.
     */
    void onCurrentChanged( const QModelIndex&, const QModelIndex& );

    /**
     * Update the Timeline when anim changes.
     */
    void useAnim( class SkeletonComponent *comp, int id );

  signals:
    /**
     * Emitted when data need redraw.
     */
    void setContinuousUpdate( bool b );

    /**
     * Emitted when data need redraw.
     */
    void askForUpdate();

  private slots:
    void onShowWeights( bool on );
    void onShowWeightsType( int type );

  private:
    /// The AnimationSystem.
    class SkeletonBasedAnimationSystem* m_system{nullptr};

    /// The Animation widget.
    SkeletonBasedAnimationUI* m_widget{nullptr};

    /// The SelectionManager of the Viewer.
    Ra::GuiBase::SelectionManager* m_selectionManager{nullptr};
};

} // namespace SkeletonBasedAnimationPlugin

#endif // SKELETONBASEDANIMATIONPLUGIN_PLUGIN_HPP_
