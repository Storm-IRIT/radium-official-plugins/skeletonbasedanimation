#ifndef SKELETONBASEDANIMATIONPLUGINMACROS_HPP_
#define SKELETONBASEDANIMATIONPLUGINMACROS_HPP_

#include <Core/CoreMacros.hpp>

/// Defines the correct macro to export dll symbols.
#if defined SkeletonBasedAnimation_EXPORTS
#    define SKEL_ANIM_PLUGIN_API DLL_EXPORT
#else
#    define SKEL_ANIM_PLUGIN_API DLL_IMPORT
#endif

#endif // SKELETONBASEDANIMATIONPLUGINMACROS_HPP_
