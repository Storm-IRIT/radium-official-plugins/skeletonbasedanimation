#ifndef SKELETONBASEDANIMATIONPLUGIN_SYSTEM_HPP_
#define SKELETONBASEDANIMATIONPLUGIN_SYSTEM_HPP_

#include <Engine/System/System.hpp>

#include <Engine/ItemModel/ItemEntry.hpp>
#include <SkeletonBasedAnimationPluginMacros.hpp>

namespace Ra::GuiBase {
class Timeline;
}

namespace SkeletonBasedAnimationPlugin {

/**
 * The SkeletonBasedAnimationSystem manages both SkeletonComponents and SkinningComponents.
 * It is also responsible for transmitting calls to/from animation-related processes.
 */
class SKEL_ANIM_PLUGIN_API SkeletonBasedAnimationSystem : public Ra::Engine::System
{
  public:
    /// Create a new animation system
    SkeletonBasedAnimationSystem();

    SkeletonBasedAnimationSystem( const SkeletonBasedAnimationSystem& ) = delete;
    SkeletonBasedAnimationSystem& operator=( const SkeletonBasedAnimationSystem& ) = delete;

    /// \name System Interface
    /// \{

    /**
     * Creates a task for each AnimationComponent to update skeleton display.
     */
    void generateTasks( Ra::Core::TaskQueue* taskQueue,
                        const Ra::Engine::FrameInfo& frameInfo ) override;

    /**
     * Loads Skeletons and Animations from a file data into the givn Entity.
     */
    void handleAssetLoading( Ra::Engine::Entity* entity,
                             const Ra::Core::Asset::FileData* fileData ) override;
    /// \}

    /// \name Skeleton display
    /// \{

    /**
     * Sets bone display xray mode to \p on for all AnimationComponents.
     */
    void setXray( bool on );

    /**
     * \returns true if bone display xray mode on, false otherwise.
     */
    bool isXrayOn();

    /**
     * Toggles skeleton display for all AnimationComponents.
     */
    void toggleSkeleton( const bool status );
    /// \}

    /// \name Animation parameters
    /// \{

    /**
     * Update the timeline w.r.t. the \p id-th animation of component \p comp.
     */
    void useAnim( class SkeletonComponent* comp, int id );

    /**
     * Sets the animation speed factor for all AnimationComponents.
     */
    void setAnimationSpeed( const Scalar value );

    /**
     * Toggles animation auto repeat for all AnimationComponents.
     */
    void autoRepeat( const bool status );

    /**
     * Toggles animation ping-pong for all AnimationComponents.
     */
    void pingPong( const bool status );

    /**
     * \returns the animation time of the AnimationComponent corresponding to \p entry 's entity.
     */
    Scalar getAnimationTime( const Ra::Engine::ItemEntry& entry ) const;
    /// \}

    /**
     * Set the Timeline to register Keyframes into.
     */
    void setTimeline( Ra::GuiBase::Timeline* timeline ) { m_timeline = timeline; }

    /// Enable display of skinning weights.
    void showWeights( bool on );

    /// Sets the type of skinning weights to display: 0 - standard, 1 - stbs.
    void showWeightsType( int type );

  private:
    /// True if we want to show xray-bones.
    bool m_xrayOn{false};

    /// The current animation time.
    Scalar m_time{0};

    /// The timeline.
    Ra::GuiBase::Timeline* m_timeline{nullptr};
};
} // namespace SkeletonBasedAnimationPlugin

#endif // SKELETONBASEDANIMATIONPLUGIN_SYSTEM_HPP_
