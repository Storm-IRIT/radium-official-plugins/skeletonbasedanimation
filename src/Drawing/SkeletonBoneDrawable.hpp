#ifndef ANIMPLUGIN_SKELETON_BONE_DRAWABLE_HPP_
#define ANIMPLUGIN_SKELETON_BONE_DRAWABLE_HPP_

#include <Core/Animation/Skeleton.hpp>

#include <Engine/Entity/Entity.hpp>
#include <Engine/Renderer/Mesh/Mesh.hpp>
#include <Engine/Renderer/RenderObject/RenderObject.hpp>
#include <Engine/Renderer/RenderTechnique/RenderTechnique.hpp>

#include <SkeletonComponent.hpp>

//#define K_VERSION

namespace SkeletonBasedAnimationPlugin {
class SkeletonBoneRenderObject
{
  public:
    SkeletonBoneRenderObject( const std::string& name,
                              SkeletonComponent* comp,
                              uint id );

    /**
     * Update local transform of the RenderObject w.r.t. the skeleton pose.
     */
    void update();

    /**
    ￼ * Returns the index of the bone.
    ￼ */
    uint getBoneIndex() const { return m_id; }

    /**
    ￼ * Returns the index of the RO used to render the bone.
    ￼ */
    Ra::Core::Utils::Index getRenderObjectIndex() const;

    /**
     * Toggle on/off xray mode to display the bone.
     */
    void setXray( bool on );

    /**
     * \returns true if xray mode is active, false otherwise.
     */
    bool isXray() const;

    /**
     * Toggle on/off display of the bone.
     */
    void setVisible( bool on );

    /**
     * \returns true if display is active, false otherwise.
     */
    bool isVisible() const;

  private:
    /**
   ￼  * Creates a mesh used to display a bone.
   ￼  */
    static Ra::Core::Geometry::TriangleMesh makeBoneShape();

  private:
    /// The RenderObject for the bone.
    Ra::Engine::RenderObject* m_ro;

    /// The index of the bone.
    uint m_id;

    /// The animation Skeleton.
    const Ra::Core::Animation::Skeleton& m_skel;
};

} // namespace SkeletonBasedAnimationPlugin

#endif // ANIMPLUGIN_SKELETON_BONE_DRAWABLE_HPP_
