#include <Drawing/SkeletonBoneDrawable.hpp>

#include <Core/Geometry/Normal.hpp>

#include <Core/Containers/MakeShared.hpp>
#include <Engine/Renderer/Material/BlinnPhongMaterial.hpp>
#include <Engine/Renderer/RenderObject/Primitives/DrawPrimitives.hpp>
#include <Engine/Renderer/RenderObject/RenderObject.hpp>
#include <Engine/Renderer/RenderObject/RenderObjectManager.hpp>

namespace SkeletonBasedAnimationPlugin {

SkeletonBoneRenderObject::SkeletonBoneRenderObject( const std::string& name,
                                                    SkeletonComponent* comp,
                                                    uint id ) :
    m_ro( nullptr ),
    m_id( id ),
    m_skel( comp->getSkeleton() ) {

    // create the mesh
    auto displayMesh = std::make_shared<Ra::Engine::Mesh>( name );
    displayMesh->loadGeometry( makeBoneShape() );

    // create the material
    auto mat  = Ra::Core::make_shared<Ra::Engine::BlinnPhongMaterial>( "Bone Material" );
    mat->m_kd = Ra::Core::Utils::Color( 0.4f, 0.4f, 0.4f, 1.f );
    mat->m_ks = Ra::Core::Utils::Color( 0.0f, 0.0f, 0.0f, 1.0f );
    m_ro      = Ra::Engine::RenderObject::createRenderObject( name,
                                                         comp,
                                                         Ra::Engine::RenderObjectType::Geometry,
                                                         displayMesh,
                                                         Ra::Engine::RenderTechnique {} );
    m_ro->setTransparent( false );
    m_ro->setMaterial( mat );
    m_ro->setXRay( false );

    comp->addRenderObject( m_ro );
    update();
}

void SkeletonBoneRenderObject::update() {
    Ra::Core::Vector3 start;
    Ra::Core::Vector3 end;
    m_skel.getBonePoints( m_id, start, end );

    Ra::Core::Transform scale = Ra::Core::Transform::Identity();
    scale.scale( ( end - start ).norm() );

    Ra::Core::Quaternion rot =
        Ra::Core::Quaternion::FromTwoVectors( Ra::Core::Vector3::UnitZ(), end - start );

    Ra::Core::Transform boneTransform =
        m_skel.getTransform( m_id, Ra::Core::Animation::HandleArray::SpaceType::MODEL );
    Ra::Core::Matrix3 rotation = rot.toRotationMatrix();
    Ra::Core::Transform drawTransform;
    drawTransform.linear()      = rotation;
    drawTransform.translation() = boneTransform.translation();

    m_ro->setLocalTransform( drawTransform * scale );
}

Ra::Core::Geometry::TriangleMesh SkeletonBoneRenderObject::makeBoneShape() {
    // Bone along Z axis.

    Ra::Core::Geometry::TriangleMesh mesh;

    const Scalar l = 0.1f;
    const Scalar w = 0.1f;

    mesh.setVertices( {Ra::Core::Vector3( 0, 0, 0 ),
                       Ra::Core::Vector3( 0, 0, 1 ),
                       Ra::Core::Vector3( 0, w, l ),
                       Ra::Core::Vector3( w, 0, l ),
                       Ra::Core::Vector3( 0, -w, l ),
                       Ra::Core::Vector3( -w, 0, l )} );

    mesh.setIndices( {Ra::Core::Vector3ui( 0, 2, 3 ),
                      Ra::Core::Vector3ui( 0, 5, 2 ),
                      Ra::Core::Vector3ui( 0, 3, 4 ),
                      Ra::Core::Vector3ui( 0, 4, 5 ),
                      Ra::Core::Vector3ui( 1, 3, 2 ),
                      Ra::Core::Vector3ui( 1, 2, 5 ),
                      Ra::Core::Vector3ui( 1, 4, 3 ),
                      Ra::Core::Vector3ui( 1, 5, 4 )} );

    Ra::Core::Geometry::uniformNormal(
        mesh.verticesWithLock(), mesh.getIndices(), mesh.normalsWithLock() );
    mesh.verticesUnlock();
    mesh.normalsUnlock();
    return mesh;
}

Ra::Core::Utils::Index SkeletonBoneRenderObject::getRenderObjectIndex() const {
    return m_ro->getIndex();
}

void SkeletonBoneRenderObject::setXray( bool on ) {
    m_ro->setXRay( on );
}

bool SkeletonBoneRenderObject::isXray() const {
    return m_ro->isXRay();
}

void SkeletonBoneRenderObject::setVisible( bool on ) {
    m_ro->setVisible( on );
}

bool SkeletonBoneRenderObject::isVisible() const {
    return m_ro->isVisible();
}

} // namespace SkeletonBasedAnimationPlugin
